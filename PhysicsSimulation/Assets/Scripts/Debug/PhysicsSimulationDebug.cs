﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 * Visual Debug
*/
public class PhysicsSimulationDebug : MonoBehaviour
{

    //float deltaTime = 0.0f;
    public int FrameRate;
    public Text FPSText;

    // Start is called before the first frame update
    void Start()
    { }

    // Update is called once per frame
    void Update()
    {
        float current = 0;
        current = (int)(1f / Time.unscaledDeltaTime);
        FrameRate = (int)current;
        FPSText.text = "FPS: " + FrameRate.ToString() + " fps";
        
       //deltaTime += (Time.unscaledDeltaTime - deltaTime) * 0.1f;
    }


  
    //void OnGUI()
    //{
    //    float fps = 1.0f / deltaTime;
    //    string fpsText = string.Format("({1:0.} fps", fps);
    //    // GUI Box Position X: 10, Y: 10  
    //    GUI.Box(new Rect(10, 10, 250, 100), "Stats");
    //    // GUI Label (Text) Position X: 30, Y: 30
    //    GUI.Label(new Rect(30, 30, 50, 20), "FPS");
    //    // GUI Label (Text) Position X:30, Y: 50 
    //} 



}
