﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Collision for the Spheres which can be hit
 */

public class PhysHitSphere : MonoBehaviour
{
    /*
     *Variables
     */

    //Unique Identifier for the SphereCollider if more than one is used.
    public int HitSphereId;

    /*
     * Physics
     */

    //The Spheres Mass
    public float Mass = 1.0f;

    /*
     * Vector3
     */

    // Force = Mass x Acceleration
    public Vector3 Force = new Vector3(0.0f, 0.0f, 0.0f);
    public Vector3 Acceleration = new Vector3(0.0f, 0.0f, 0.0f);
    public Vector3 Velocity = new Vector3(0.0f, 0.0f, 0.0f);




    // Start is called before the first frame update
    void Start()
    {
    }
    // Update is called once per frame
    void Update()
    {
    }
}
