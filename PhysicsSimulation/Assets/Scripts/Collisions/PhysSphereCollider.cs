﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Collision for the NormalSphere
 * 
 * No Collision
 */

public class PhysSphereCollider : MonoBehaviour
{
    /*
     *Variables
     */

    //Unique Identifier for the SphereCollider if more than one is used.
    public int SphereStartID;
    

    // Start is called before the first frame update
    void Start()
    {   
    }
    // Update is called once per frame
    void Update()
    { 
    }
}
