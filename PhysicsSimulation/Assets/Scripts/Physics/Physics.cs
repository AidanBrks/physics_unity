﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * Physics Resolver
 */

public class Physics : MonoBehaviour
{
    /*
     *  Declare and Initialise Variables
     */
    // Gravity
    public float Gravity = -9.8f;
    // Mass
    public float Mass = 1.0f;
    // Velocity
    public Vector3 Velocity;
    // Force + Radius
    public float Force, SphereRadius;

    /* 
     * Time
     */
    //DeltaTime -  provides the time between the current and previous frame
    // Current Time - The Current Time
    // Other Time - Previous Time
    public float DeltaTime = 0, CurrentTime = 0 , OtherTime = 0;
    
    /*
     * Game Objects + Unique Id's for Objects
     */
    // Singular Sphere (Initator)
    public GameObject StartSphere;
    // Array of GameObjects Sphere
    // Size 15 - Can be Hit + Start (Initator) Spheres
    public GameObject[] HitSphere;
    //GameObject Floor
    public GameObject Floor;
    public int PlaneID = 0, HSID = 0, StartSphereID = 0;
    /*
     * Distances 
     */

    //Max Distances
    public float MaxSphereDistance, MaxPlaneDistance;
    //Normal Distances
    public float PlaneDistance, SphereDistance;
    
    //Called before the first frame update
    void Start()
    {
        // Console Output - To show console that C# script is loaded
        Debug.Log("Physics Script Loaded");

        /*
         * Variable Setup
         */

        //Distance
        MaxSphereDistance = 1;
        MaxPlaneDistance = (SphereRadius + Floor.transform.localScale.y / 2);
        //Find HitSpheres in GameScenes.
        foreach (GameObject hitSphere in HitSphere)
        {
            // Loop through and Give an Unique Identifier to the Sphere which can be hit
            hitSphere.GetComponent<PhysHitSphere>().HitSphereId = HSID;
            // Increment the Variable ID so the Objects get Unique Identifer
            HSID++;
        }
    }

    //Called once per frame
    void Update()
    {
        // Scale to see how fast time is going
        if (Time.timeScale == 1)
        {
            //Time
            CurrentTime = Time.realtimeSinceStartup;
            DeltaTime = CurrentTime - OtherTime;
            OtherTime = CurrentTime;

            // Input Check - UPArrow on the Keyboard
            // Check if User has Pressed
            //Increase the Velocity
            if (Input.GetKey(KeyCode.UpArrow))
            {
                //Debug.Log("Input Check: Up Arrow is Selected");
                StartSphere.GetComponent<PhysHitSphere>().Velocity.x += -0.5f;
            }
            foreach (GameObject hitSphere in HitSphere)
            {
                PhysCalcVelocity(hitSphere);
                PhysCalcAcceleration(hitSphere);
                PhysMoveObject(hitSphere);
                // Slow down Unity Object called HitSphere if X and Y Velocity is Less than 0.05, set back to 0
                if (((hitSphere.GetComponent<PhysHitSphere>().Velocity.x) * (hitSphere.GetComponent<PhysHitSphere>().Velocity.x) + (hitSphere.GetComponent<PhysHitSphere>().Velocity.y) * (hitSphere.GetComponent<PhysHitSphere>().Velocity.y)) < 0.05f)
                {
                    hitSphere.GetComponent<PhysHitSphere>().Velocity = new Vector3(0.0f, 0.0f, 0.0f);
                }
            }
            //Check if Collision has Happened
            PhysCheckObjectPosition();
            CurrentTime += DeltaTime;
        }
    }

    /*
     * Functions
     */

    // Calculate the Velocity of a the GameObject in X,Y
    void PhysCalcVelocity(GameObject ob)
    {
        ob.GetComponent<PhysHitSphere>().Velocity.x += ob.GetComponent<PhysHitSphere>().Acceleration.x * DeltaTime;
        ob.GetComponent<PhysHitSphere>().Velocity.y += ob.GetComponent<PhysHitSphere>().Acceleration.y * DeltaTime;
    } 
    //Calculate the Acceleration of a GameObject in X,Y
    void PhysCalcAcceleration(GameObject ob)
    {
        ob.GetComponent<PhysHitSphere>().Acceleration.x = -(ob.GetComponent<PhysHitSphere>().Velocity.x * 0.8f);
        ob.GetComponent<PhysHitSphere>().Acceleration.y = -(ob.GetComponent<PhysHitSphere>().Velocity.y * 0.8f);
    }
    //Calculate the Acceleration of a GameObject in X,Y
    // A = F / M
    void PhysCalcAccelerationDueToForce()
    {
        StartSphere.GetComponent<PhysHitSphere>().Acceleration.x = (StartSphere.GetComponent<PhysHitSphere>().Force.x / Mass);
        StartSphere.GetComponent<PhysHitSphere>().Acceleration.y = (StartSphere.GetComponent<PhysHitSphere>().Force.y / Mass);
    }
    //Move the Objects after Calculating the Velocity and Acceleration
    void PhysMoveObject(GameObject ob)
    {
        //Move the Object along the X Axsis
        ob.transform.position += new Vector3((ob.GetComponent<PhysHitSphere>().Velocity.x * DeltaTime), 0.0f, 0.0f);
        //Move the Object along the Y Axsis
        ob.transform.position += new Vector3(0.0f, 0.0f, (ob.GetComponent<PhysHitSphere>().Velocity.y * DeltaTime));
    }
    void PhysCalcObjectInitialForce()
    {
        StartSphere.GetComponent<PhysHitSphere>().Force = (new Vector3(-9.0f, 0, 0));
    }


    void PhysCalcDynamic(GameObject ObjectOne, GameObject ObjectTwo, float DistanceB)
    {
        float NormalX = (ObjectTwo.transform.position.x - ObjectOne.transform.position.x) / DistanceB;
        float NormalY = (ObjectTwo.transform.position.z - ObjectOne.transform.position.z) / DistanceB;
        float TangentX = -NormalY;
        float TangentY = NormalX;
        //Dot Product
        float TangentialDotProd1 = ObjectOne.GetComponent<PhysHitSphere>().Velocity.x * TangentX + ObjectOne.GetComponent<PhysHitSphere>().Velocity.y * TangentY;
        float TangentialDotProd2 = ObjectTwo.GetComponent<PhysHitSphere>().Velocity.x * TangentX + ObjectTwo.GetComponent<PhysHitSphere>().Velocity.y * TangentY;
        float NormalDotProd1 = ObjectOne.GetComponent<PhysHitSphere>().Velocity.x * NormalX + ObjectOne.GetComponent<PhysHitSphere>().Velocity.y * NormalY;
        float NormalDotProd2 = ObjectTwo.GetComponent<PhysHitSphere>().Velocity.x * NormalX + ObjectTwo.GetComponent<PhysHitSphere>().Velocity.y * NormalX;
        // Momentum = mass X velocity
        float Momentum1 = (NormalDotProd1 * (ObjectOne.GetComponent<PhysHitSphere>().Mass - ObjectTwo.GetComponent<PhysHitSphere>().Mass) + 2.0f * ObjectTwo.GetComponent<PhysHitSphere>().Mass * NormalDotProd2) / ObjectOne.GetComponent<PhysHitSphere>().Mass * ObjectTwo.GetComponent<PhysHitSphere>().Mass;
        float Momentum2 = (NormalDotProd2 * (ObjectTwo.GetComponent<PhysHitSphere>().Mass - ObjectOne.GetComponent<PhysHitSphere>().Mass) + 2.0f * ObjectOne.GetComponent<PhysHitSphere>().Mass * NormalDotProd1) / ObjectOne.GetComponent<PhysHitSphere>().Mass * ObjectTwo.GetComponent<PhysHitSphere>().Mass;
        //Velocity Update
        ObjectOne.GetComponent<PhysHitSphere>().Velocity.x = TangentX * TangentialDotProd1 + NormalX * Momentum1;
        ObjectOne.GetComponent<PhysHitSphere>().Velocity.y = TangentY * TangentialDotProd1 + NormalY * Momentum1;
        ObjectTwo.GetComponent<PhysHitSphere>().Velocity.x = TangentX * TangentialDotProd2 + NormalX * Momentum2;
        ObjectTwo.GetComponent<PhysHitSphere>().Velocity.y = TangentY * TangentialDotProd2 + NormalY * Momentum2;
    }

    //Function to Check object positions
    public void PhysCheckObjectPosition()
    {
        //Loop through the Object initator and the objects which can be hit
        foreach (GameObject hitSphere in HitSphere)
        {
            foreach (GameObject OtherObject in HitSphere)
            {
                //If Id doesn't equal the same id run
                if (hitSphere.GetComponent<PhysHitSphere>().HitSphereId != OtherObject.GetComponent<PhysHitSphere>().HitSphereId)
                {
                    //MathF.abs - returns the absolute value of value
                    //Vector3.Distance - returns distance between object a and b
                    SphereDistance = Mathf.Abs(Vector3.Distance(hitSphere.transform.position, OtherObject.transform.position));
                    if (SphereDistance <= MaxSphereDistance)
                    {
                        // SquareRoot Position x Position
                        float DistanceA = Mathf.Sqrt((hitSphere.transform.position.x - OtherObject.transform.position.x) * (hitSphere.transform.position.x - OtherObject.transform.position.x) + (hitSphere.transform.position.z - OtherObject.transform.position.z) * (hitSphere.transform.position.z - OtherObject.transform.position.z));
                        PhysCalcDynamic(hitSphere, OtherObject, DistanceA);
                        float DistanceOV = 0.5f * (DistanceA - 1.0f);
                        hitSphere.transform.position -= new Vector3(DistanceOV * (hitSphere.transform.position.x - OtherObject.transform.position.x) / DistanceA, 0, DistanceOV * (hitSphere.transform.position.z - OtherObject.transform.position.z) / DistanceA);
                        OtherObject.transform.position += new Vector3(DistanceOV * (hitSphere.transform.position.x - OtherObject.transform.position.x) / DistanceA, 0, DistanceOV * (hitSphere.transform.position.z - OtherObject.transform.position.z) / DistanceA);
                    }
                }
            }
        }
    }
}
